module.exports = {
	darkMode: "class",
	theme: {
		// darkSelector: '.dark-mode',
		fontFamily: {
			sans: ["Inter", "Arial"],
		},
		container: {
			center: true,
		},
		colors: {
			test: "#123456",
			black: "#05060a",
			white: "#FFFFFF",
			gray: {
				darkest: "#151b26",
				dark: "#363d47",
				default: "#767c87",
				light: "#d3d6db",
				lightest: "#f2f5f7",
			},
			primary: "#0039de",
			primaryLight: "#388bff",
			secondary: "#ff0000",
			transparent: "#ffffff00",
		},
		extend: {
			spacing: {
				72: "18rem",
				76: "19rem",
				80: "20rem",
				84: "21rem",
				96: "24rem",
				128: "32rem",
			},
		},
	},
};
